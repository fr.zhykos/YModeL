/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.services.parsers;

import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

/**
 * Service to parse a YML file
 *
 * @param <R> Type returned by the parser
 */
public abstract class AbstractYmlParsingService<R> {

    /**
     * Parse the saved YML string
     *
     * @param yamlContent The string to parse
     * @return An object (typed as see in method {@link #getParsingTargetClass()})
     * @throws SyntaxException Syntax exception
     */
    public R parse(final String yamlContent) throws SyntaxException {
        try {
            final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            return mapper.readValue(yamlContent, getParsingTargetClass());
        } catch (final UnrecognizedPropertyException e) {
            final String message = "Unrecognized property '%s' at line %d and column %d."
                .formatted(e.getPropertyName(), e.getLocation().getLineNr(), e.getLocation().getColumnNr());
            throw new SyntaxException(message);
        } catch (final MismatchedInputException e) {
            final String message = "Wrong syntax for '%s' at line %d and column %d."
                .formatted(
                        e.getPath().stream().map(Reference::getFieldName).collect(Collectors.joining(", ", "[", "]")),
                        e.getLocation().getLineNr(), e.getLocation().getColumnNr());
            throw new SyntaxException(message);
        } catch (final Exception e) {
            throw new SyntaxException(e);
        }
    }

    protected abstract Class<R> getParsingTargetClass();

}
