/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.models.definition.generators;

import lombok.Getter;
import lombok.Setter;

/**
 * A generator definition
 */
public class DefinitionGenerator {

    /**
     * ID to identify a native generator. Use it with the {@link #service}
     * attribute.
     */
    public static final String NATIVE_GENERATOR = "$native";

    /**
     * Pretty name
     */
    @Getter
    @Setter
    private String name;

    /**
     * Template name (mustache file)
     */
    @Getter
    @Setter
    private String template;

    /**
     * Transformation service name: the qualified Java class name (not the full
     * package: root is "fr.zhykos.ymodel.domain.services" which is not required)
     */
    @Getter
    @Setter
    private String service;

}
