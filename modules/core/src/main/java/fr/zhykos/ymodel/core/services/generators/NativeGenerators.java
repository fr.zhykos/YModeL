/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.services.generators;

import java.util.Optional;

import fr.zhykos.ymodel.domain.models.ITemplateClass;
import fr.zhykos.ymodel.domain.services.IGenerationService;
import fr.zhykos.ymodel.domain.services.typescript.GenerationTypeScriptService;

/**
 * Native generators
 */
public final class NativeGenerators {

    private NativeGenerators() {
        // Static class
    }

    /**
     * Create a registered native generator
     *
     * @param <C>  C: the type of the generated class
     * @param <M>  M: the type of the generated method
     * @param name Generator name
     * @return The specific generator
     */
    public static <C extends ITemplateClass, M> Optional<IGenerationService<C, M>> createGenerator(final String name) {
        @SuppressWarnings("unchecked")
        final IGenerationService<C, M> generator = (IGenerationService<C, M>) switch (name) {
            case "TypeScript" -> new GenerationTypeScriptService();
            default -> null;
        };
        return Optional.ofNullable(generator);
    }

}
