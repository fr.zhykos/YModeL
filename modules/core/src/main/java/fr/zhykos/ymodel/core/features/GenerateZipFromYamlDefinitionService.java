/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.features;

import java.io.OutputStream;
import java.util.Optional;

import fr.zhykos.ymodel.commons.log.tasks.ITasksLogger;
import fr.zhykos.ymodel.commons.telemetry.ITelemetry;
import fr.zhykos.ymodel.core.TechnicalException;
import fr.zhykos.ymodel.core.models.definition.generators.DefinitionGenerator;
import fr.zhykos.ymodel.core.models.definition.generators.DefinitionGeneratorFile;
import fr.zhykos.ymodel.core.services.definitionparsers.DefinitionYmlParsingService;
import fr.zhykos.ymodel.core.services.generators.DefinitionGeneratorsParsingService;
import fr.zhykos.ymodel.core.services.generators.GeneratorsService;
import fr.zhykos.ymodel.core.services.postgeneration.ZipGeneratedFilesService;
import fr.zhykos.ymodel.core.services.transformers.TransformYmlDefinitionService;
import fr.zhykos.ymodel.domain.models.ITemplateClass;
import fr.zhykos.ymodel.domain.services.IGenerationService;
import lombok.AllArgsConstructor;

/**
 * Main feature: generate a zip file containing generated files defined by a YML
 * file
 */
@AllArgsConstructor
public final class GenerateZipFromYamlDefinitionService {

    /**
     * Internal telemetry for traces, logs and metrics
     */
    private final ITelemetry telemetry;

    /**
     * Internal logs for tasks (services for instance): used to log start and stop
     * tasks
     */
    private final ITasksLogger tasksLogger;

    /**
     * Main feature: generate a zip file containing generated files defined by a YML
     * file
     *
     * @param yamlContent    The definition content to parse
     * @param targetLanguage Target language for generated files
     * @param outputStream   Target stream in which write the zip
     * @throws TechnicalException Technical error
     */
    public void generate(final String yamlContent, final String targetLanguage, final OutputStream outputStream)
            throws TechnicalException {
        final var definitionParser = new DefinitionYmlParsingService();
        final var transformer = new TransformYmlDefinitionService();
        final IGenerationService<? extends ITemplateClass, ?> generationService = createGenerator(targetLanguage);
        final var postGenerationService = new ZipGeneratedFilesService(outputStream);

        YMFFeature
            .execute(yamlContent, definitionParser, transformer, generationService, postGenerationService,
                    this.telemetry, this.tasksLogger);
    }

    private IGenerationService<? extends ITemplateClass, ?> createGenerator(final String targetLanguage)
            throws TechnicalException {
        try {
            final DefinitionGeneratorFile parseResult = new DefinitionGeneratorsParsingService().parse();
            final Optional<DefinitionGenerator> findFirstGenerator = parseResult
                .getGenerators()
                .stream()
                .filter(generator -> generator.getName().equals(targetLanguage))
                .findFirst();
            if (findFirstGenerator.isEmpty()) {
                throw new TechnicalException(
                        "Cannot find definition generator with name '%s'.".formatted(targetLanguage));
            }
            final DefinitionGenerator definitionGenerator = findFirstGenerator.get();
            return GeneratorsService.createGenerator(definitionGenerator);
        } catch (final Exception e) {
            throw new TechnicalException(e);
        }
    }

}
