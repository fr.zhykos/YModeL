/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.services.generators;

import java.lang.reflect.Constructor;

import fr.zhykos.ymodel.core.models.definition.generators.DefinitionGenerator;
import fr.zhykos.ymodel.domain.models.ITemplateClass;
import fr.zhykos.ymodel.domain.services.IGenerationService;

/**
 * Service to create the specific generator
 */
public final class GeneratorsService {

    private GeneratorsService() {
        // Do nothing
    }

    /**
     * Create the specific generator
     *
     * @param definitionGenerator Definition of the generator to create
     * @return The generator
     * @throws GeneratorsException Error while creating the generator
     */
    public static IGenerationService<ITemplateClass, Object> createGenerator(
            final DefinitionGenerator definitionGenerator) throws GeneratorsException {
        final String serviceName = definitionGenerator.getService();
        if (DefinitionGenerator.NATIVE_GENERATOR.equals(serviceName)) {
            return NativeGenerators
                .createGenerator(definitionGenerator.getName())
                .orElseThrow(() -> new GeneratorsException(
                        "Cannot find native generator named: '%s'.".formatted(definitionGenerator.getName())));
        } else {
            return dynamicGenerator(serviceName);
        }
    }

    private static IGenerationService<ITemplateClass, Object> dynamicGenerator(final String serviceName)
            throws GeneratorsException {
        try {
            final String service = serviceName.replace('/', '.');
            final Class<?> serviceClass = Class.forName("fr.zhykos.ymodel.domain.services." + service);
            final Constructor<?> serviceConstructor = serviceClass.getConstructor();
            @SuppressWarnings("unchecked")
            final IGenerationService<ITemplateClass, Object> generator = (IGenerationService<ITemplateClass, Object>) serviceConstructor
                .newInstance();
            return generator;
        } catch (final Exception e) {
            throw new GeneratorsException(e);
        }
    }

    /**
     * Exception while creating the specific generator
     */
    public static final class GeneratorsException extends Exception {

        /**
         * New exception
         *
         * @param cause Exception cause
         */
        private GeneratorsException(final Throwable cause) {
            super(cause);
        }

        /**
         * New exception
         *
         * @param message Exception message
         */
        private GeneratorsException(final String message) {
            super(message);
        }

    }

}
