/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.features;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;

import fr.zhykos.ymodel.commons.log.tasks.ITaskLog;
import fr.zhykos.ymodel.commons.log.tasks.ITasksLogger;
import fr.zhykos.ymodel.commons.telemetry.IChildSpan;
import fr.zhykos.ymodel.commons.telemetry.ITelemetry;
import fr.zhykos.ymodel.core.TechnicalException;
import fr.zhykos.ymodel.core.models.GeneratedFile;
import fr.zhykos.ymodel.core.models.definition.IDefinitionMetamodel;
import fr.zhykos.ymodel.core.services.definitionparsers.IDefinitionParser;
import fr.zhykos.ymodel.core.services.generation.GenerationService;
import fr.zhykos.ymodel.core.services.generation.GenerationService.GenerationException;
import fr.zhykos.ymodel.core.services.parsers.SyntaxException;
import fr.zhykos.ymodel.core.services.postgeneration.IPostGenerationService;
import fr.zhykos.ymodel.core.services.transformers.ITransformer;
import fr.zhykos.ymodel.core.services.transformers.TransformYmlDefinitionService.SemanticListException;
import fr.zhykos.ymodel.domain.models.ITemplateClass;
import fr.zhykos.ymodel.domain.services.IGenerationService;

/**
 * Execute any main feature:
 * <ol>
 * <li>parse a file
 * <li>transform the file model into EMF
 * <li>generate files
 * <li>optionally post process generated files
 */
public final class YMFFeature {

    /**
     * Tasks total
     */
    private static final int TASKS_TOTAL = 4;

    private YMFFeature() {
        // Do nothing
    }

    /**
     * Execute a complete feature
     *
     * @param <M>                       The metamodel type to transform
     *
     * @param sourceContent             Source content to parse, transform then
     *                                  generate
     * @param definitionParser          Definition parser
     * @param transformer               Transformer from definition file to EMF
     * @param specificGenerationService File generator
     * @param postGenerationService     Service to execute after the generation
     * @param telemetry                 Internal telemetry for traces, logs and
     *                                  metrics
     * @param tasksLogger               Internal logs for tasks (services for
     *                                  instance): used to log start and stop tasks
     * @throws TechnicalException Technical error
     */
    public static <M extends IDefinitionMetamodel> void execute(final String sourceContent,
            final IDefinitionParser definitionParser, final ITransformer<M> transformer,
            final IGenerationService<? extends ITemplateClass, ?> specificGenerationService,
            final IPostGenerationService postGenerationService, final ITelemetry telemetry,
            final ITasksLogger tasksLogger) throws TechnicalException {
        try {
            int taskNumber = 0;
            final M definitionMetamodel = parse(sourceContent, definitionParser, telemetry, tasksLogger, ++taskNumber);
            final List<EClass> eClasses = transform(definitionMetamodel, transformer, telemetry, tasksLogger,
                    ++taskNumber);
            final List<GeneratedFile> generatedFiles = generate(eClasses, specificGenerationService, telemetry,
                    tasksLogger, ++taskNumber);
            postGenerate(postGenerationService, generatedFiles, telemetry, tasksLogger, ++taskNumber);
        } catch (final Exception e) {
            telemetry.recordException(e);
            throw new TechnicalException(e);
        }
    }

    private static <M extends IDefinitionMetamodel> M parse(final String sourceContent,
            final IDefinitionParser definitionParser, final ITelemetry telemetry, final ITasksLogger tasksLogger,
            final int taskNumber) throws SyntaxException {
        try (IChildSpan span = telemetry.createChildSpan("Parse metamodel"); ITaskLog taskLog = tasksLogger.start()) {
            definitionParser.identifySpan(span);
            definitionParser.identifyTaskLogger(taskLog, taskNumber, TASKS_TOTAL);
            @SuppressWarnings("unchecked")
            final M cast = (M) definitionParser.parse(sourceContent).getMetamodel();
            return cast;
        }
    }

    private static <M extends IDefinitionMetamodel> List<EClass> transform(final M definitionMetamodel,
            final ITransformer<M> transformer, final ITelemetry telemetry, final ITasksLogger tasksLogger,
            final int taskNumber) throws SemanticListException, SyntaxException {
        try (IChildSpan span = telemetry.createChildSpan("Transform metamodel");
             ITaskLog taskLog = tasksLogger.start()) {
            transformer.identifySpan(span);
            transformer.identifyTaskLogger(taskLog, taskNumber, TASKS_TOTAL);
            return transformer.transform(definitionMetamodel);
        }
    }

    private static List<GeneratedFile> generate(final List<EClass> eClasses,
            final IGenerationService<? extends ITemplateClass, ?> specificGenerationService, final ITelemetry telemetry,
            final ITasksLogger tasksLogger, final int taskNumber) throws GenerationException {
        final List<GeneratedFile> generatedFiles = new ArrayList<>();
        for (final EClass eClass : eClasses) {
            generatedFiles.add(generate(eClass, specificGenerationService, telemetry, tasksLogger, taskNumber));
        }
        return generatedFiles;
    }

    private static GeneratedFile generate(final EClass eClass,
            final IGenerationService<? extends ITemplateClass, ?> specificGenerationService, final ITelemetry telemetry,
            final ITasksLogger tasksLogger, final int taskNumber) throws GenerationException {
        try (IChildSpan span = telemetry.createChildSpan("Generate file"); ITaskLog taskLog = tasksLogger.start()) {
            final GenerationService generationService = new GenerationService();
            generationService
                .identifySpan(span,
                        Map
                            .of("Generated class", eClass.getName(), "Generation language ID",
                                    specificGenerationService.getTemplateName()));
            generationService.identifyTaskLogger(taskLog, taskNumber, TASKS_TOTAL);
            return generationService.generate(eClass, specificGenerationService);
        }
    }

    private static void postGenerate(final IPostGenerationService postGenerationService,
            final List<GeneratedFile> generatedFiles, final ITelemetry telemetry, final ITasksLogger tasksLogger,
            final int taskNumber) throws TechnicalException {
        try (IChildSpan span = telemetry.createChildSpan("Execute treatments on generated files");
             ITaskLog taskLog = tasksLogger.start()) {
            postGenerationService.identifySpan(span);
            postGenerationService.identifyTaskLogger(taskLog, taskNumber, TASKS_TOTAL);
            postGenerationService.execute(generatedFiles);
        }
    }

}
