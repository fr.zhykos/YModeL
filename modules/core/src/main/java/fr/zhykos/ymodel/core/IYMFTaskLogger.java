/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core;

import fr.zhykos.ymodel.commons.log.tasks.IIdentifiedTaskLogger;
import fr.zhykos.ymodel.commons.log.tasks.ITaskLog;

/**
 * A YMF task logger
 */
public interface IYMFTaskLogger extends IIdentifiedTaskLogger {

    @Override
    default void identifyTaskLogger(final ITaskLog taskLog, final int taskNumber, final int tasksTotal) {
        final String service = identifyTaskService();
        if (!service.isEmpty()) {
            taskLog.setMessage("[%d / %d] %s".formatted(taskNumber, tasksTotal, identifyTaskService()));
        }
    }

    /**
     * Used to identify the service as a task.
     * Do not implement this method unless you want to add the name in the current
     * logger.
     *
     * @return The current service name or empty to prevent adding the name in the
     *         logger messages
     */
    default String identifyTaskService() {
        return "";
    }

}
