/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core;

import java.util.Map;

import fr.zhykos.ymodel.commons.telemetry.IChildSpan;
import fr.zhykos.ymodel.commons.telemetry.IIdentifiedSpan;

/**
 * A YMF tracer Span
 */
public interface IYMFSpan extends IIdentifiedSpan {

    @Override
    default void identifySpan(final IChildSpan span, final Map<String, String> params) {
        final String service = identifySpanService();
        if (!service.isEmpty()) {
            span.setAttribute("YMF service", service);
        }
        params.forEach(span::setAttribute);
    }

    /**
     * Used to identify the service in the current Span.
     * Do not implement this method unless you want to add the name in the Span.
     *
     * @return The current service name or empty to prevent adding the name in the
     *         Span
     */
    default String identifySpanService() {
        return "";
    }

}
