/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.services.transformers;

import java.util.List;

import org.eclipse.emf.ecore.EClass;

import fr.zhykos.ymodel.core.IYMFSpan;
import fr.zhykos.ymodel.core.IYMFTaskLogger;
import fr.zhykos.ymodel.core.models.definition.IDefinitionMetamodel;
import fr.zhykos.ymodel.core.services.parsers.SyntaxException;
import fr.zhykos.ymodel.core.services.transformers.TransformYmlDefinitionService.SemanticListException;

/**
 * Transform an internal metamodel into EMF
 *
 * @param <M> The metamodel type to transform
 */
public interface ITransformer<M extends IDefinitionMetamodel> extends IYMFSpan, IYMFTaskLogger {

    /**
     * Transform an internal metamodel into EMF
     *
     * @param metamodel The metamodel to transform
     * @return List of transformer EMF classes
     * @throws SemanticListException All semantic exceptions
     * @throws SyntaxException       Syntax error
     */
    List<EClass> transform(M metamodel) throws SemanticListException, SyntaxException;

}
