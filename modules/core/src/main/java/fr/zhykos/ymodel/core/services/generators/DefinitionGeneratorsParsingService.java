/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.services.generators;

import java.io.IOException;

import fr.zhykos.ymodel.core.models.definition.generators.DefinitionGeneratorFile;
import fr.zhykos.ymodel.core.services.parsers.AbstractYmlParsingService;
import fr.zhykos.ymodel.core.services.parsers.SyntaxException;
import fr.zhykos.ymodel.domain.internal.ResourceFileHelper;
import lombok.AllArgsConstructor;

/**
 * Service to parse the generators definition file
 */
@AllArgsConstructor
public final class DefinitionGeneratorsParsingService extends AbstractYmlParsingService<DefinitionGeneratorFile> {

    /**
     * Content to parse
     */
    private final String yamlContent;

    /**
     * Constructor: use the internal definitions
     *
     * @throws IOException Error while reading the file
     */
    public DefinitionGeneratorsParsingService() throws IOException {
        this(ResourceFileHelper.readResourceFile("generators.yml"));
    }

    /**
     * Parse the generators definition file
     *
     * @return The generators definition models
     * @throws SyntaxException Syntax exception
     */
    public DefinitionGeneratorFile parse() throws SyntaxException {
        return super.parse(this.yamlContent);
    }

    @Override
    protected Class<DefinitionGeneratorFile> getParsingTargetClass() {
        return DefinitionGeneratorFile.class;
    }

}
