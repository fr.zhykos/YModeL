/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.services.generation;

import java.io.StringReader;
import java.io.StringWriter;

import org.eclipse.emf.ecore.EClass;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import fr.zhykos.ymodel.core.IYMFSpan;
import fr.zhykos.ymodel.core.IYMFTaskLogger;
import fr.zhykos.ymodel.core.models.GeneratedFile;
import fr.zhykos.ymodel.domain.models.ITemplateClass;
import fr.zhykos.ymodel.domain.services.IGenerationService;

/**
 * Generic service to generate files
 */
public final class GenerationService implements IYMFSpan, IYMFTaskLogger {

    /**
     * Generate a class
     *
     * @param <C>                       C: the type of the generated class. Must
     *                                  implements
     *                                  {@link ITemplateClass}
     * @param <M>                       M: the type of the generated method
     *
     * @param eClass                    The class to generate
     * @param specificGenerationService Specific service to generate the classes:
     *                                  language
     *                                  service target
     * @return A {@link GeneratedFile} with the generated content
     * @throws GenerationException Generation exception if an error occurred.
     */
    public <C extends ITemplateClass, M> GeneratedFile generate(final EClass eClass,
            final IGenerationService<C, M> specificGenerationService) throws GenerationException {
        final C generatedClass;
        if (eClass.getESuperTypes().isEmpty()) {
            generatedClass = specificGenerationService.generateClass(eClass.getName());
        } else {
            final String inherits = eClass.getESuperTypes().get(0).getName();
            generatedClass = specificGenerationService.generateClass(eClass.getName(), inherits);
        }
        eClass.getEOperations().forEach(operation -> {
            final M generatedMethod = specificGenerationService
                .generateMethod(generatedClass, operation.getName(), operation.getEType().getName());

            operation
                .getEParameters()
                .forEach(parameter -> specificGenerationService
                    .generateMethodParameter(generatedClass, generatedMethod, parameter.getName(),
                            parameter.getEType().getName()));

            specificGenerationService.postMethodGeneration(generatedMethod);
        });
        eClass
            .getEAttributes()
            .forEach(attribute -> specificGenerationService
                .generateField(generatedClass, attribute.getName(), attribute.getEType().getName()));
        return executeTemplate(generatedClass, specificGenerationService);
    }

    private static <C extends ITemplateClass, M> GeneratedFile executeTemplate(final C templateClass,
            final IGenerationService<C, M> specificGenerationService) throws GenerationException {
        try {
            final MustacheFactory mustacheFactory = new DefaultMustacheFactory();
            final Mustache mustache = mustacheFactory
                .compile(new StringReader(specificGenerationService.getTemplateContents()),
                        specificGenerationService.getTemplateName());
            final StringWriter stringWriter = new StringWriter();
            mustache.execute(stringWriter, templateClass).flush();
            return new GeneratedFile(templateClass.getFileClassName(), stringWriter.toString().replace("&#39;", "'"));
        } catch (final Exception exception) {
            throw new GenerationException(exception);
        }
    }

    @Override
    public String identifySpanService() {
        return "Generate a metamodel file";
    }

    @Override
    public String identifyTaskService() {
        return identifySpanService();
    }

    /**
     * Exception while generating a new Class
     */
    public static final class GenerationException extends Exception {

        /**
         * New Exception caused by...
         *
         * @param cause The cause
         */
        private GenerationException(final Throwable cause) {
            super(cause);
        }

    }

}
