/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.zhykos.ymodel.core.models.definition.yaml.DefinitionYmlClass;
import fr.zhykos.ymodel.core.models.definition.yaml.DefinitionYmlFile;
import fr.zhykos.ymodel.core.models.definition.yaml.DefinitionYmlMetamodel;
import fr.zhykos.ymodel.core.models.definition.yaml.DefinitionYmlMethod;
import fr.zhykos.ymodel.core.models.definition.yaml.DefinitionYmlMethodParameter;
import fr.zhykos.ymodel.core.services.definitionparsers.DefinitionYmlParsingService;
import fr.zhykos.ymodel.core.services.parsers.SyntaxException;

class DefinitionParsingServiceTests {

    @Test
    void parse() throws SyntaxException, IOException {
        final String yamlFile = Files.readString(Path.of("src/test/resources/metamodel01.yml"));
        final DefinitionYmlFile parseReturns = new DefinitionYmlParsingService().parse(yamlFile);
        final DefinitionYmlMetamodel metamodel = parseReturns.getMetamodel();

        Assertions.assertEquals(2, metamodel.getClasses().size());

        final DefinitionYmlClass class01 = metamodel.getClasses().get(0);
        Assertions.assertNull(class01.getInherits());
        Assertions.assertEquals("Class01", class01.getName());
        Assertions.assertEquals(2, class01.getMethods().size());

        final DefinitionYmlMethod method01 = class01.getMethods().get(0);
        Assertions.assertEquals("method01", method01.getName());
        Assertions.assertEquals("void", method01.getReturns());
        Assertions.assertEquals(2, method01.getParameters().size());

        final DefinitionYmlMethodParameter param01 = method01.getParameters().get(0);
        Assertions.assertEquals("param01", param01.getName());
        Assertions.assertEquals("int", param01.getType());

        final DefinitionYmlMethodParameter param02 = method01.getParameters().get(1);
        Assertions.assertEquals("param02", param02.getName());
        Assertions.assertEquals("string", param02.getType());

        final DefinitionYmlMethod method02 = class01.getMethods().get(1);
        Assertions.assertEquals("method02", method02.getName());
        Assertions.assertEquals("float", method02.getReturns());
        Assertions.assertEquals(0, method02.getParameters().size());

        final DefinitionYmlClass class02 = metamodel.getClasses().get(1);
        Assertions.assertEquals("$Class01", class02.getInherits());
        Assertions.assertEquals("Class02", class02.getName());
        Assertions.assertEquals(1, class02.getMethods().size());

        final DefinitionYmlMethod method03 = class02.getMethods().get(0);
        Assertions.assertEquals("method03", method03.getName());
        Assertions.assertEquals("char", method03.getReturns());
        Assertions.assertEquals(1, method03.getParameters().size());

        final DefinitionYmlMethodParameter param03 = method03.getParameters().get(0);
        Assertions.assertEquals("param03", param03.getName());
        Assertions.assertEquals("int", param03.getType());
    }

    @Test
    void unrecognizedPropertyException() throws IOException {
        final String yamlFile = Files.readString(Path.of("src/test/resources/metamodel04.yml"));
        Assertions.assertThrows(SyntaxException.class, () -> new DefinitionYmlParsingService().parse(yamlFile));
    }

    @Test
    void mismatchedInputException() throws IOException {
        final String yamlFile = Files.readString(Path.of("src/test/resources/metamodel05.yml"));
        Assertions.assertThrows(SyntaxException.class, () -> new DefinitionYmlParsingService().parse(yamlFile));

        try {
            new DefinitionYmlParsingService().parse(yamlFile);
        } catch (SyntaxException e) {
            Assertions.assertEquals("Wrong syntax for '[metamodel, classes]' at line 11 and column 5.", e.getMessage());
        }
    }

}
