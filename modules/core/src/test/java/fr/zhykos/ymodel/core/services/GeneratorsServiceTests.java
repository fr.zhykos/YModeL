/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.services;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.zhykos.ymodel.core.models.definition.generators.DefinitionGenerator;
import fr.zhykos.ymodel.core.services.generators.GeneratorsService;
import fr.zhykos.ymodel.core.services.generators.GeneratorsService.GeneratorsException;
import fr.zhykos.ymodel.domain.models.ITemplateClass;
import fr.zhykos.ymodel.domain.services.IGenerationService;
import fr.zhykos.ymodel.domain.services.typescript.GenerationTypeScriptService;

class GeneratorsServiceTests {

    @Test
    void createGenerator() throws GeneratorsException {
        // Given
        final DefinitionGenerator definitionGenerator = new DefinitionGenerator();
        definitionGenerator.setName("TypeScript");
        definitionGenerator.setTemplate("typescript");
        definitionGenerator.setService(DefinitionGenerator.NATIVE_GENERATOR);

        // When
        final IGenerationService<? extends ITemplateClass, ?> generator = GeneratorsService
            .createGenerator(definitionGenerator);
        final ITemplateClass generatedClass = generator.generateClass("foo");

        // Then
        MatcherAssert.assertThat(generator, Matchers.instanceOf(GenerationTypeScriptService.class));
        Assertions.assertEquals("foo", generatedClass.getName());
        Assertions.assertFalse(generatedClass.hasImports());
    }

    @Test
    void createNotNativeGenerator() throws GeneratorsException {
        // Given
        final DefinitionGenerator definitionGenerator = new DefinitionGenerator();
        definitionGenerator.setName("TypeScript");
        definitionGenerator.setTemplate("typescript");
        definitionGenerator.setService("typescript/GenerationTypeScriptService");

        // When
        final IGenerationService<? extends ITemplateClass, ?> generator = GeneratorsService
            .createGenerator(definitionGenerator);
        final ITemplateClass generatedClass = generator.generateClass("foo");

        // Then
        MatcherAssert.assertThat(generator, Matchers.instanceOf(GenerationTypeScriptService.class));
        Assertions.assertEquals("foo", generatedClass.getName());
        Assertions.assertFalse(generatedClass.hasImports());
    }

    @Test
    void absentNative() throws GeneratorsException {
        // Given
        final DefinitionGenerator definitionGenerator = new DefinitionGenerator();
        definitionGenerator.setName("foo");
        definitionGenerator.setTemplate("typescript");
        definitionGenerator.setService(DefinitionGenerator.NATIVE_GENERATOR);

        // Then
        Assertions
            .assertThrows(GeneratorsException.class, () -> GeneratorsService.createGenerator(definitionGenerator));
    }

    @Test
    void dynamicException() throws GeneratorsException {
        // Given
        final DefinitionGenerator definitionGenerator = new DefinitionGenerator();
        definitionGenerator.setTemplate("typescript");

        // Then
        Assertions
            .assertThrows(GeneratorsException.class, () -> GeneratorsService.createGenerator(definitionGenerator));
    }

}
