/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.zhykos.ymodel.core.models.definition.generators.DefinitionGenerator;
import fr.zhykos.ymodel.core.models.definition.generators.DefinitionGeneratorFile;
import fr.zhykos.ymodel.core.services.generators.DefinitionGeneratorsParsingService;
import fr.zhykos.ymodel.core.services.parsers.SyntaxException;

class GeneratorsParsingServiceTests {

    @Test
    void parse() throws SyntaxException, IOException {
        final String yamlFile = Files.readString(Path.of("src/test/resources/generators01.yml"));
        final DefinitionGeneratorFile generators = new DefinitionGeneratorsParsingService(yamlFile).parse();

        Assertions.assertEquals(2, generators.getGenerators().size());

        final DefinitionGenerator generator01 = generators.getGenerators().get(0);
        Assertions.assertEquals("TypeScript", generator01.getName());
        Assertions.assertEquals("typescript", generator01.getTemplate());
        Assertions.assertEquals("typescript/GenerationTypeScriptService", generator01.getService());

        final DefinitionGenerator generator02 = generators.getGenerators().get(1);
        Assertions.assertEquals("Java", generator02.getName());
        Assertions.assertEquals("java", generator02.getTemplate());
        Assertions.assertEquals("typescript/GenerationJavaService", generator02.getService());
    }

    @Test
    void transform() throws IOException {
        final String yamlFile = Files.readString(Path.of("src/test/resources/generators02.yml"));
        Assertions.assertThrows(SyntaxException.class, () -> new DefinitionGeneratorsParsingService(yamlFile).parse());

        try {
            new DefinitionGeneratorsParsingService(yamlFile).parse();
        } catch (SyntaxException e) {
            Assertions.assertEquals("Unrecognized property 'error' at line 3 and column 22.", e.getMessage());
        }
    }

}
