/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.services;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.zip.ZipException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;

import fr.zhykos.ymodel.commons.zip.ZipFile;
import fr.zhykos.ymodel.commons.zip.ZipHelper;
import fr.zhykos.ymodel.core.TechnicalException;
import fr.zhykos.ymodel.core.models.GeneratedFile;
import fr.zhykos.ymodel.core.services.postgeneration.ZipGeneratedFilesService;

class ZipResultServiceTests {

    @Test
    void zip() throws IOException, ZipException, TechnicalException {
        final FileSystem fileSystem = Jimfs.newFileSystem(Configuration.unix());

        final List<GeneratedFile> generatedFiles = List
            .of(new GeneratedFile("file01.txt", "foo"), new GeneratedFile("file02.txt", "hello there"));

        final Path junitPath = fileSystem.getPath("junit");
        Files.createDirectory(junitPath);
        final Path zipPath = Files.createTempFile(junitPath, "result", ".zip");
        try (OutputStream outputStream = Files.newOutputStream(zipPath)) {
            new ZipGeneratedFilesService(outputStream).execute(generatedFiles);
        }

        Assertions
            .assertEquals(generatedFiles.stream().map(GeneratedFile::getContents).toList(),
                    ZipHelper.unzip(zipPath).stream().map(ZipFile::getContents).toList());
        Assertions
            .assertEquals(generatedFiles.stream().map(GeneratedFile::getFilename).toList(),
                    ZipHelper.unzip(zipPath).stream().map(ZipFile::getFilename).toList());
    }

    @Test
    void zipException() throws IOException {
        final List<GeneratedFile> generatedFiles = List.of(new GeneratedFile("file01.txt", "foo"));
        try (OutputStream outputStream = Mockito.mock(OutputStream.class)) {
            Mockito.doThrow(IOException.class).when(outputStream).write(Mockito.anyInt());
            Assertions
                .assertThrows(TechnicalException.class,
                        () -> new ZipGeneratedFilesService(outputStream).execute(generatedFiles));
        }
    }

}
