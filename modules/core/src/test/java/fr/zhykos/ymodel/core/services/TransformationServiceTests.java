/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.core.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.zhykos.ymodel.core.models.definition.yaml.DefinitionYmlClass;
import fr.zhykos.ymodel.core.models.definition.yaml.DefinitionYmlField;
import fr.zhykos.ymodel.core.models.definition.yaml.DefinitionYmlFile;
import fr.zhykos.ymodel.core.models.definition.yaml.DefinitionYmlMetamodel;
import fr.zhykos.ymodel.core.models.definition.yaml.DefinitionYmlMethod;
import fr.zhykos.ymodel.core.models.definition.yaml.DefinitionYmlMethodParameter;
import fr.zhykos.ymodel.core.services.definitionparsers.DefinitionYmlParsingService;
import fr.zhykos.ymodel.core.services.parsers.SyntaxException;
import fr.zhykos.ymodel.core.services.transformers.TransformYmlDefinitionService;
import fr.zhykos.ymodel.core.services.transformers.TransformYmlDefinitionService.SemanticListException;

class TransformationServiceTests {

    @Test
    void transform() throws SemanticListException {
        final DefinitionYmlMetamodel ymlMetamodel = createMetamodel();
        final List<EClass> eClasses = new TransformYmlDefinitionService().transform(ymlMetamodel);
        Assertions.assertEquals(2, eClasses.size());

        final EClass class01 = eClasses.get(0);
        Assertions.assertEquals(0, class01.getESuperTypes().size());
        Assertions.assertEquals("Class01", class01.getName());
        Assertions.assertEquals(2, class01.getEOperations().size());

        final EOperation method01 = class01.getEOperations().get(0);
        Assertions.assertEquals("method01", method01.getName());
        Assertions.assertEquals("void", method01.getEType().getName());
        Assertions.assertEquals(2, method01.getEParameters().size());

        final EParameter param01 = method01.getEParameters().get(0);
        Assertions.assertEquals("param01", param01.getName());
        Assertions.assertEquals("int", param01.getEType().getName());

        final EParameter param02 = method01.getEParameters().get(1);
        Assertions.assertEquals("param02", param02.getName());
        Assertions.assertEquals("string", param02.getEType().getName());

        final EOperation method02 = class01.getEOperations().get(1);
        Assertions.assertEquals("method02", method02.getName());
        Assertions.assertEquals("float", method02.getEType().getName());
        Assertions.assertEquals(0, method02.getEParameters().size());

        final EClass class02 = eClasses.get(1);
        Assertions.assertEquals(1, class02.getESuperTypes().size());
        Assertions.assertEquals(class01, class02.getESuperTypes().get(0));
        Assertions.assertEquals("Class02", class02.getName());
        Assertions.assertEquals(1, class02.getEOperations().size());

        final EOperation method03 = class02.getEOperations().get(0);
        Assertions.assertEquals("method03", method03.getName());
        Assertions.assertEquals("char", method03.getEType().getName());
        Assertions.assertEquals(1, method03.getEParameters().size());

        final EParameter param03 = method03.getEParameters().get(0);
        Assertions.assertEquals("param03", param03.getName());
        Assertions.assertEquals("int", param03.getEType().getName());
    }

    private static DefinitionYmlMetamodel createMetamodel() {
        final DefinitionYmlClass class01 = new DefinitionYmlClass();
        class01.setName("Class01");

        final DefinitionYmlMethod method01 = new DefinitionYmlMethod();
        method01.setName("method01");
        method01.setReturns("void");

        final DefinitionYmlMethodParameter param01 = new DefinitionYmlMethodParameter();
        param01.setName("param01");
        param01.setType("int");

        final DefinitionYmlMethodParameter param02 = new DefinitionYmlMethodParameter();
        param02.setName("param02");
        param02.setType("string");

        final DefinitionYmlMethod method02 = new DefinitionYmlMethod();
        method02.setName("method02");
        method02.setReturns("float");

        final DefinitionYmlClass class02 = new DefinitionYmlClass();
        class02.setName("Class02");
        class02.setInherits("$Class01");

        final DefinitionYmlMethod method03 = new DefinitionYmlMethod();
        method03.setName("method03");
        method03.setReturns("char");

        final DefinitionYmlMethodParameter param03 = new DefinitionYmlMethodParameter();
        param03.setName("param03");
        param03.setType("int");

        final DefinitionYmlField field = new DefinitionYmlField("field", "string");

        final DefinitionYmlMetamodel metamodel = new DefinitionYmlMetamodel();
        metamodel.getClasses().add(class01);
        metamodel.getClasses().add(class02);
        class01.getMethods().add(method01);
        class01.getMethods().add(method02);
        class02.getMethods().add(method03);
        class02.getFields().add(field);
        method01.getParameters().add(param01);
        method01.getParameters().add(param02);
        method03.getParameters().add(param03);
        return metamodel;
    }

    @Test
    void transformUnreferencedTypes() throws SyntaxException, IOException {
        final String yamlFile = Files.readString(Path.of("src/test/resources/metamodel03.yml"));
        final DefinitionYmlFile parseReturns = new DefinitionYmlParsingService().parse(yamlFile);
        final DefinitionYmlMetamodel ymlMetamodel = parseReturns.getMetamodel();

        Assertions
            .assertThrows(SemanticListException.class,
                    () -> new TransformYmlDefinitionService().transform(ymlMetamodel));

        try {
            new TransformYmlDefinitionService().transform(ymlMetamodel);
        } catch (SemanticListException e) {
            Assertions
                .assertEquals(
                        "Transformation error:%n - Unknown class reference for inheritance: $Class01%n - Unknown class reference '$Class01' in element 'field01'%n - Unknown class reference '$Class01' in element 'method01'%n - Unknown class reference '$Class01' in element 'param01'."
                            .formatted(yamlFile),
                        e.getMessage());
        }
    }

    @Test
    void transformReferencedTypes() throws SemanticListException, SyntaxException, IOException {
        final String yamlFile = Files.readString(Path.of("src/test/resources/metamodel02.yml"));
        final DefinitionYmlFile parseReturns = new DefinitionYmlParsingService().parse(yamlFile);
        final DefinitionYmlMetamodel ymlMetamodel = parseReturns.getMetamodel();

        final List<EClass> eClasses = new TransformYmlDefinitionService().transform(ymlMetamodel);
        Assertions.assertEquals(2, eClasses.size());

        final EClass class01 = eClasses.get(0);
        Assertions.assertEquals(0, class01.getESuperTypes().size());
        Assertions.assertEquals("Class01", class01.getName());
        Assertions.assertEquals(0, class01.getEOperations().size());
        Assertions.assertEquals(0, class01.getEAttributes().size());

        final EClass class02 = eClasses.get(1);
        Assertions.assertEquals(0, class02.getESuperTypes().size());
        Assertions.assertEquals("Class02", class02.getName());
        Assertions.assertEquals(1, class02.getEOperations().size());

        final EOperation method01 = class02.getEOperations().get(0);
        Assertions.assertEquals("method01", method01.getName());
        Assertions.assertEquals(class01, method01.getEType());
        Assertions.assertEquals(1, method01.getEParameters().size());

        final EParameter param03 = method01.getEParameters().get(0);
        Assertions.assertEquals("param01", param03.getName());
        Assertions.assertEquals(class01, param03.getEType());
    }

}
