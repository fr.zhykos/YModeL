/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.domain.services.typescript;

import java.util.List;
import java.util.SortedSet;

import fr.zhykos.ymodel.domain.models.typescript.TypeScriptClass;
import fr.zhykos.ymodel.domain.models.typescript.TypeScriptField;
import fr.zhykos.ymodel.domain.models.typescript.TypeScriptMethod;
import fr.zhykos.ymodel.domain.models.typescript.TypeScriptMethodParameter;
import fr.zhykos.ymodel.domain.services.IGenerationService;

/**
 * Service to generate TypeScript files
 */
public final class GenerationTypeScriptService implements IGenerationService<TypeScriptClass, TypeScriptMethod> {

    @Override
    public String getTemplateName() {
        return "typescript";
    }

    @Override
    public TypeScriptClass generateClass(final String className) {
        final TypeScriptClass typeScriptClass = new TypeScriptClass();
        typeScriptClass.setName(className);
        return typeScriptClass;
    }

    @Override
    public TypeScriptClass generateClass(final String className, final String inheritedClassName) {
        final TypeScriptClass typeScriptClass = generateClass(className);
        typeScriptClass.setInherits(inheritedClassName);
        typeScriptClass.getImports().add(inheritedClassName);
        return typeScriptClass;
    }

    @Override
    public void generateField(final TypeScriptClass containingClass, final String fieldName, final String fieldType) {
        final TypeScriptField typeScriptMethod = new TypeScriptField();
        typeScriptMethod.setName(fieldName);
        typeScriptMethod.setType(mapTypeScriptType(fieldType, containingClass.getImports()));
        containingClass.getFields().add(typeScriptMethod);
    }

    @Override
    public TypeScriptMethod generateMethod(final TypeScriptClass containingClass, final String methodName,
            final String methodReturnsType) {
        final TypeScriptMethod typeScriptMethod = new TypeScriptMethod();
        typeScriptMethod.setName(methodName);
        typeScriptMethod.setReturns(mapTypeScriptType(methodReturnsType, containingClass.getImports()));
        containingClass.getMethods().add(typeScriptMethod);
        return typeScriptMethod;
    }

    @Override
    public void postMethodGeneration(final TypeScriptMethod createdMethod) {
        if (!createdMethod.getParameters().isEmpty()) {
            createdMethod.getParameters().get(createdMethod.getParameters().size() - 1).setLast(true);
        }
    }

    @Override
    public void generateMethodParameter(final TypeScriptClass containingClass, final TypeScriptMethod containingMethod,
            final String parameterName, final String parameterType) {
        final TypeScriptMethodParameter typeScriptMethodParameter = new TypeScriptMethodParameter();
        typeScriptMethodParameter.setName(parameterName);
        typeScriptMethodParameter.setType(mapTypeScriptType(parameterType, containingClass.getImports()));
        containingMethod.getParameters().add(typeScriptMethodParameter);
    }

    private static String mapTypeScriptType(final String type, final SortedSet<String> imports) {
        return switch (type) {
            case "int", "float" -> "number";
            case "char", "string" -> "string";
            default -> mapTypeScriptOtherTypes(type, imports);
        };
    }

    private static String mapTypeScriptOtherTypes(final String type, final SortedSet<String> imports) {
        final List<String> dontImport = List.of("void");
        if (!dontImport.contains(type)) {
            imports.add(type);
        }
        return type;
    }

}
