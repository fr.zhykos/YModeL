/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.domain.internal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

/**
 * Resource file helper
 */
public final class ResourceFileHelper {

    private ResourceFileHelper() {
        // Do nothing
    }

    /**
     * Read a resource file with is in the current module
     *
     * @param filename File name
     * @return The content of the file
     * @throws IOException Error while reading the file
     */
    public static String readResourceFile(final String filename) throws IOException {
        try (InputStream inputStream = ResourceFileHelper.class.getClassLoader().getResourceAsStream(filename);
             var streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
             var bufferReader = new BufferedReader(streamReader)) {
            return bufferReader.lines().collect(Collectors.joining("\n"));
        } catch (final Exception e) {
            throw new IOException("Cannot read file: " + filename, e);
        }
    }

}
