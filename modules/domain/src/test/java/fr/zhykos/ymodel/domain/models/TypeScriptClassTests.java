/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.domain.models;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.zhykos.ymodel.domain.models.typescript.TypeScriptClass;

class TypeScriptClassTests {

    @Test
    void hasNoImports() {
        final TypeScriptClass typeScriptClass = new TypeScriptClass();
        Assertions.assertFalse(typeScriptClass.hasImports());
    }

    @Test
    void hasImports() {
        final TypeScriptClass typeScriptClass = new TypeScriptClass();
        typeScriptClass.getImports().add("foo");
        Assertions.assertIterableEquals(List.of("foo"), typeScriptClass.getImports());
        Assertions.assertTrue(typeScriptClass.hasImports());
    }

    @Test
    void getFileClassName() {
        final TypeScriptClass typeScriptClass = new TypeScriptClass();
        typeScriptClass.setName("foo");
        Assertions.assertEquals("foo.ts", typeScriptClass.getFileClassName());
    }

}
