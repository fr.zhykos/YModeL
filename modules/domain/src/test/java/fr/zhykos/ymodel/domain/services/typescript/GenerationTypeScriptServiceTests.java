/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.domain.services.typescript;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.zhykos.ymodel.domain.models.typescript.TypeScriptClass;
import fr.zhykos.ymodel.domain.models.typescript.TypeScriptMethod;
import fr.zhykos.ymodel.domain.models.typescript.TypeScriptMethodParameter;

class GenerationTypeScriptServiceTests {

    @Test
    void templateName() {
        Assertions.assertEquals("typescript", new GenerationTypeScriptService().getTemplateName());
    }

    @Test
    void generateClass() {
        final GenerationTypeScriptService service = new GenerationTypeScriptService();
        final TypeScriptClass typeScriptClass = service.generateClass("MyClass");
        Assertions.assertEquals("MyClass", typeScriptClass.getName());
        Assertions.assertNull(typeScriptClass.getInherits());
        Assertions.assertTrue(typeScriptClass.getImports().isEmpty());
    }

    @Test
    void generateClassWithInheritance() {
        final GenerationTypeScriptService service = new GenerationTypeScriptService();
        final TypeScriptClass typeScriptClass = service.generateClass("MyClass", "ExtendedClass");
        Assertions.assertEquals("MyClass", typeScriptClass.getName());
        Assertions.assertEquals("ExtendedClass", typeScriptClass.getInherits());
        Assertions.assertIterableEquals(List.of("ExtendedClass"), typeScriptClass.getImports());
    }

    @Test
    void generateField() {
        final GenerationTypeScriptService service = new GenerationTypeScriptService();
        final TypeScriptClass typeScriptClass = service.generateClass("MyClass");
        service.generateField(typeScriptClass, "myField", "string");
        Assertions.assertEquals(1, typeScriptClass.getFields().size());
        Assertions.assertEquals("myField", typeScriptClass.getFields().get(0).getName());
        Assertions.assertEquals("string", typeScriptClass.getFields().get(0).getType());
    }

    @Test
    void generateMethod() {
        final GenerationTypeScriptService service = new GenerationTypeScriptService();
        final TypeScriptClass typeScriptClass = service.generateClass("MyClass");
        final TypeScriptMethod typeScriptMethod = service.generateMethod(typeScriptClass, "myMethod", "string");
        Assertions.assertEquals("myMethod", typeScriptMethod.getName());
        Assertions.assertEquals("string", typeScriptMethod.getReturns());
        Assertions.assertTrue(typeScriptMethod.getParameters().isEmpty());
        Assertions.assertTrue(typeScriptClass.getImports().isEmpty());
    }

    @Test
    void generateMethodWithParameters() {
        final GenerationTypeScriptService service = new GenerationTypeScriptService();
        final TypeScriptClass typeScriptClass = service.generateClass("MyClass");
        final TypeScriptMethod typeScriptMethod = service.generateMethod(typeScriptClass, "myMethod", "string");
        service.generateMethodParameter(typeScriptClass, typeScriptMethod, "string", "string");
        Assertions.assertEquals("myMethod", typeScriptMethod.getName());
        Assertions.assertEquals("string", typeScriptMethod.getReturns());
        final List<TypeScriptMethodParameter> parameters = typeScriptMethod.getParameters();
        Assertions.assertEquals(1, parameters.size());
        Assertions.assertEquals("string", parameters.get(0).getName());
        Assertions.assertEquals("string", parameters.get(0).getType());
    }

    @Test
    void postMethodGeneration() {
        final GenerationTypeScriptService service = new GenerationTypeScriptService();
        final TypeScriptClass typeScriptClass = service.generateClass("MyClass");
        final TypeScriptMethod typeScriptMethod = service.generateMethod(typeScriptClass, "myMethod", "string");
        service.postMethodGeneration(typeScriptMethod);
        final List<TypeScriptMethodParameter> parameters = typeScriptMethod.getParameters();
        Assertions.assertEquals(0, parameters.size());
    }

    @Test
    void postMethodGenerationSetLast() {
        final GenerationTypeScriptService service = new GenerationTypeScriptService();
        final TypeScriptClass typeScriptClass = service.generateClass("MyClass");
        final TypeScriptMethod typeScriptMethod = service.generateMethod(typeScriptClass, "myMethod", "string");
        service.generateMethodParameter(typeScriptClass, typeScriptMethod, "string1", "string1");
        service.generateMethodParameter(typeScriptClass, typeScriptMethod, "string2", "string2");
        service.postMethodGeneration(typeScriptMethod);
        final List<TypeScriptMethodParameter> parameters = typeScriptMethod.getParameters();
        Assertions.assertEquals(2, parameters.size());
        Assertions.assertFalse(parameters.get(0).isLast());
        Assertions.assertTrue(parameters.get(1).isLast());
    }

    @Test
    void generateNumberField() {
        final GenerationTypeScriptService service = new GenerationTypeScriptService();
        final TypeScriptClass typeScriptClass = service.generateClass("MyClass");
        service.generateField(typeScriptClass, "myField", "int");
        Assertions.assertEquals(1, typeScriptClass.getFields().size());
        Assertions.assertEquals("number", typeScriptClass.getFields().get(0).getType());
    }

    @Test
    void generateVoidMethod() {
        final GenerationTypeScriptService service = new GenerationTypeScriptService();
        final TypeScriptClass typeScriptClass = service.generateClass("MyClass");
        final TypeScriptMethod typeScriptMethod = service.generateMethod(typeScriptClass, "myMethod", "void");
        Assertions.assertEquals("myMethod", typeScriptMethod.getName());
        Assertions.assertEquals("void", typeScriptMethod.getReturns());
        Assertions.assertTrue(typeScriptMethod.getParameters().isEmpty());
        Assertions.assertTrue(typeScriptClass.getImports().isEmpty());
    }

}
