/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.telemetry;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import fr.zhykos.ymodel.commons.telemetry.impl.NoTelemetry;
import fr.zhykos.ymodel.commons.telemetry.impl.OpenTelemetryChildSpan;
import fr.zhykos.ymodel.commons.telemetry.impl.OpenTelemetryForOtlpGrpcExporter;
import io.opentelemetry.api.GlobalOpenTelemetry;

class TelemetryBuilderTests {

    private static final String FOO = "foo";

    @AfterEach
    public void afterEach() {
        GlobalOpenTelemetry.resetForTest();
    }

    @Test
    void noTelemetry() {
        final ITelemetry noTelemetry = TelemetryBuilder.disable().build();
        MatcherAssert.assertThat(noTelemetry, Matchers.instanceOf(NoTelemetry.class));

        final IChildSpan span = noTelemetry.createChildSpan(FOO);
        MatcherAssert
            .assertThat(span.getClass().getName(),
                    Matchers.startsWith("fr.zhykos.ymodel.commons.telemetry.impl.NoTelemetry$"));

        Assertions.assertDoesNotThrow(() -> span.close());

        final Exception mockException = Mockito.mock(Exception.class);
        noTelemetry.recordException(mockException);
        Mockito.verifyNoInteractions(mockException);

        // Setting an attribute after closing the span is wrong if it is a valid span
        // (not from NoTelemetry)
        Assertions.assertDoesNotThrow(() -> span.setAttribute("key", "value"));
    }

    @Test
    void noTelemetryWrongConfig() {
        Assertions.assertThrows(NullPointerException.class, () -> TelemetryBuilder.otlpGrpc(null));
    }

    @Test
    void otlpGrpc() {
        final ITelemetry telemetry = TelemetryBuilder.otlpGrpc("http://localhost").setTracerSettings(FOO, FOO).build();
        MatcherAssert.assertThat(telemetry, Matchers.instanceOf(OpenTelemetryForOtlpGrpcExporter.class));

        final IChildSpan span = telemetry.createChildSpan(FOO);
        MatcherAssert.assertThat(span, Matchers.instanceOf(OpenTelemetryChildSpan.class));

        Assertions.assertDoesNotThrow(() -> span.close());

        final Exception mockException = Mockito.mock(Exception.class);
        telemetry.recordException(mockException);
        Mockito.verifyNoInteractions(mockException);

        Assertions.assertThrows(IllegalStateException.class, () -> span.setAttribute("key", "value"));
    }

}
