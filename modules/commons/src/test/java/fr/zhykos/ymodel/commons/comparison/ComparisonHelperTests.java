package fr.zhykos.ymodel.commons.comparison;

import java.nio.file.Path;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ComparisonHelperTests {

    @Test
    void compareStringEqualsFileContentsAsExcepted() {
        final Path filepath = Path.of("src/test/resources/foo.txt");
        final IComparisonResult comparison = ComparisonHelper
            .compareStringEqualsFileContentsAsExcepted("hello!", filepath);
        MatcherAssert.assertThat(comparison, Matchers.instanceOf(ComparisonOK.class));
    }

    @Test
    void compareStringEqualsFileContentsAsExceptedNotEqual() {
        final Path filepath = Path.of("src/test/resources/foo.txt");
        final IComparisonResult comparison = ComparisonHelper
            .compareStringEqualsFileContentsAsExcepted("fail", filepath);
        MatcherAssert.assertThat(comparison, Matchers.instanceOf(NotEqual.class));
        Assertions.assertEquals("NotEqual(actual=fail, expected=hello!)", comparison.toString());
        Assertions.assertNotEquals(((NotEqual) comparison).getExpected(), ((NotEqual) comparison).getActual());
    }

    @Test
    void compareStringEqualsFileContentsAsExceptedFail() {
        final Path filepath = Path.of("fake.txt");
        final IComparisonResult comparison = ComparisonHelper
            .compareStringEqualsFileContentsAsExcepted("fail", filepath);
        MatcherAssert.assertThat(comparison, Matchers.instanceOf(Fail.class));
        Assertions.assertEquals("fake.txt", comparison.toString());
    }

}
