/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.log.tasks;

import java.util.stream.Collectors;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.zhykos.ymodel.commons.log.tasks.impl.ConsoleTaskLog;
import fr.zhykos.ymodel.commons.log.tasks.impl.ConsoleTasksLogger;
import fr.zhykos.ymodel.commons.log.tasks.impl.NoTasksLogger;
import fr.zhykos.ymodel.commons.tests.AbstractTestJavaLogger;

class TasksLoggerBuilderTests extends AbstractTestJavaLogger {

    protected TasksLoggerBuilderTests() {
        super(ConsoleTasksLogger.LOGGER_ID);
    }

    @BeforeEach
    public void beforeEach() {
        replaceHandlers();
    }

    @AfterEach
    public void afterEach() {
        resetHandlers();
    }

    @Test
    void noLogger() {
        final ITasksLogger logger = TasksLoggerBuilder.disable().build();
        MatcherAssert.assertThat(logger, Matchers.instanceOf(NoTasksLogger.class));

        final ITaskLog log = logger.start();
        MatcherAssert
            .assertThat(log.getClass().getName(),
                    Matchers.startsWith("fr.zhykos.ymodel.commons.log.tasks.impl.NoTasksLogger$"));

        Assertions.assertDoesNotThrow(() -> log.close());
        Assertions.assertDoesNotThrow(() -> log.setMessage("message"));
    }

    @Test
    void consoleLogger() {
        final ITasksLogger logger = TasksLoggerBuilder.console().build();
        MatcherAssert.assertThat(logger, Matchers.instanceOf(ConsoleTasksLogger.class));

        final ITaskLog log = logger.start();
        MatcherAssert.assertThat(log, Matchers.instanceOf(ConsoleTaskLog.class));

        log.setMessage("hello there");
        Assertions.assertEquals("Start task: hello there", getLogMessages().stream().collect(Collectors.joining(";")));

        Assertions.assertDoesNotThrow(() -> log.close());
        Assertions.assertEquals(2, getLogMessages().size());
        Assertions.assertEquals("Start task: hello there", getLogMessages().get(0));
        MatcherAssert.assertThat(getLogMessages().get(1), Matchers.startsWith("Task ended in: "));

        Assertions.assertThrows(IllegalStateException.class, () -> log.setMessage("message"));
    }

}
