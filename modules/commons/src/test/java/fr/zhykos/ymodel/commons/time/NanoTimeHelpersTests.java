/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.time;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class NanoTimeHelpersTests {

    @Test
    void nanoToHumanForMicro() {
        final String nanoToHuman = NanoTimeHelpers.nanoToHuman(1_000);
        Assertions.assertEquals("0s 0ms 1µs 0ns", nanoToHuman);
    }

    @Test
    void nanoToHumanForNano() {
        final String nanoToHuman = NanoTimeHelpers.nanoToHuman(10);
        Assertions.assertEquals("0s 0ms 0µs 10ns", nanoToHuman);
    }

    @Test
    void nanoToHumanForMillis() {
        final String nanoToHuman = NanoTimeHelpers.nanoToHuman(1_000_000);
        Assertions.assertEquals("0s 1ms 0µs 0ns", nanoToHuman);
    }

    @Test
    void nanoToHumanForSecond() {
        final String nanoToHuman = NanoTimeHelpers.nanoToHuman(1_000_000_000);
        Assertions.assertEquals("1s 0ms 0µs 0ns", nanoToHuman);
    }

    @Test
    void nanoToHuman() {
        final String nanoToHuman = NanoTimeHelpers.nanoToHuman(1_001_001_010);
        Assertions.assertEquals("1s 1ms 1µs 10ns", nanoToHuman);
    }

}
