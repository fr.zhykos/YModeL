package fr.zhykos.ymodel.commons.zip;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ZipHelperTests {

    @Test
    void unzipPath() throws IOException {
        // When
        final List<ZipFile> unzipFiles = ZipHelper.unzip(Path.of("src/test/resources/foo.zip"));

        // Then
        Assertions.assertEquals(2, unzipFiles.size());

        final ZipFile zipFile1 = unzipFiles.get(0);
        Assertions.assertEquals("file01.txt", zipFile1.getFilename());
        Assertions.assertEquals("foo", zipFile1.getContents());

        final ZipFile zipFile2 = unzipFiles.get(1);
        Assertions.assertEquals("file02.hey", zipFile2.getFilename());
        Assertions.assertEquals("hello!", zipFile2.getContents());
    }

    @Test
    void unzipStream() throws IOException {
        // Given
        final byte[] stream = Files.readAllBytes(Path.of("src/test/resources/foo.zip"));

        // When
        final List<ZipFile> unzipFiles = ZipHelper.unzip(stream);

        // Then
        Assertions.assertEquals(2, unzipFiles.size());

        final ZipFile zipFile1 = unzipFiles.get(0);
        Assertions.assertEquals("file01.txt", zipFile1.getFilename());
        Assertions.assertEquals("foo", zipFile1.getContents());

        final ZipFile zipFile2 = unzipFiles.get(1);
        Assertions.assertEquals("file02.hey", zipFile2.getFilename());
        Assertions.assertEquals("hello!", zipFile2.getContents());
    }

}
