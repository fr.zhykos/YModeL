/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.time;

import java.util.concurrent.TimeUnit;

/**
 * Helpers for nano times (may be from {@link System#nanoTime()}).
 */
public final class NanoTimeHelpers {

    private NanoTimeHelpers() {
        // Do nothing
    }

    /**
     * Transform a nanoTime (may be from {@link System#nanoTime()}) into a human
     * readable String using letters to indicate seconds (s), milliseconds (ms),
     * etc.
     * Please note that as nanoTime is a long, there is no way to print minutes
     * because the number will be larger than Java can manage.
     *
     * @param nanoTime The time to transform
     * @return A String representing the time
     */
    public static String nanoToHuman(final long nanoTime) {
        long remaining = nanoTime;

        long seconds = TimeUnit.SECONDS.convert(remaining, TimeUnit.NANOSECONDS);
        remaining = remaining - seconds * TimeUnit.NANOSECONDS.convert(1, TimeUnit.SECONDS);

        long millis = TimeUnit.MILLISECONDS.convert(remaining, TimeUnit.NANOSECONDS);
        remaining = remaining - millis * TimeUnit.NANOSECONDS.convert(1, TimeUnit.MILLISECONDS);

        long micros = TimeUnit.MICROSECONDS.convert(remaining, TimeUnit.NANOSECONDS);
        remaining = remaining - micros * TimeUnit.NANOSECONDS.convert(1, TimeUnit.MICROSECONDS);

        return "%ds %dms %dµs %dns".formatted(seconds, millis, micros, remaining);
    }

}
