/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.log.tasks.impl;

import java.util.logging.ConsoleHandler;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

import fr.zhykos.ymodel.commons.log.tasks.ITaskLog;
import fr.zhykos.ymodel.commons.log.tasks.ITasksLogger;
import lombok.extern.java.Log;

/**
 * Console logger
 */
@Log(topic = ConsoleTasksLogger.LOGGER_ID)
public final class ConsoleTasksLogger implements ITasksLogger {

    /**
     * Logger ID
     */
    public static final String LOGGER_ID = "ConsoleTasksLogger";

    /**
     * Constructor.
     * Init logger with a specific format for the console.
     * Logger ID = the class simple name.
     */
    public ConsoleTasksLogger() {
        log.setUseParentHandlers(false);
        final ConsoleHandler handler = new ConsoleHandler();
        log.addHandler(handler);
        handler.setFormatter(new SimpleFormatter() {
            private static final String FORMAT = "%s%n";

            @Override
            public synchronized String format(final LogRecord logRecord) {
                return String.format(FORMAT, logRecord.getMessage());
            }
        });
    }

    @Override
    public ITaskLog start() {
        return new ConsoleTaskLog();
    }

}
