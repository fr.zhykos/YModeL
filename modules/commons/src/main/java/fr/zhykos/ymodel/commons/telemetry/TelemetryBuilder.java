/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.telemetry;

import java.util.Objects;

import fr.zhykos.ymodel.commons.telemetry.impl.NoTelemetry;
import fr.zhykos.ymodel.commons.telemetry.impl.OpenTelemetryForOtlpGrpcExporter;

/**
 * Telemetry builder
 */
public final class TelemetryBuilder {

    /**
     * Endpoint to the OTLP GRPC agent
     */
    private String endpoint;

    /**
     * Service name to use when tracing
     */
    private String tracerServiceName;

    /**
     * The global tracer name
     */
    private String tracerGlobalName;

    private TelemetryBuilder() {
        // Do nothing
    }

    /**
     * Create a telemetry using OpenTelemetry GRPC agent
     *
     * @param endpoint URL of the OpenTelemetry GRPC agent
     * @return A builder
     */
    public static TelemetryBuilder otlpGrpc(final String endpoint) {
        Objects.requireNonNull(endpoint, () -> "Endpoint must be set!");
        final TelemetryBuilder builder = new TelemetryBuilder();
        builder.endpoint = endpoint;
        return builder;
    }

    /**
     * A builder to disable telemetry
     *
     * @return A builder
     */
    public static TelemetryBuilder disable() {
        return new TelemetryBuilder();
    }

    /**
     * Build the telemetry object with the given settings
     *
     * @return A telemetry
     */
    public ITelemetry build() {
        if (this.endpoint == null) {
            return new NoTelemetry();
        }
        return new OpenTelemetryForOtlpGrpcExporter(this.endpoint, this.tracerServiceName, this.tracerGlobalName);
    }

    /**
     * Set tracer settings
     *
     * @param serviceName      Service name to use when tracing
     * @param globalTracerName The global tracer name
     * @return The current telemetry builder
     */
    public TelemetryBuilder setTracerSettings(final String serviceName, final String globalTracerName) {
        this.tracerServiceName = serviceName;
        this.tracerGlobalName = globalTracerName;
        return this;
    }

}
