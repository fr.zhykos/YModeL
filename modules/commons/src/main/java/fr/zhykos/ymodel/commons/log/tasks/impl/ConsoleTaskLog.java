/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.log.tasks.impl;

import fr.zhykos.ymodel.commons.log.tasks.ITaskLog;
import fr.zhykos.ymodel.commons.time.NanoTimeHelpers;
import lombok.extern.java.Log;

/**
 * Log into the console
 */
@Log(topic = ConsoleTasksLogger.LOGGER_ID)
public class ConsoleTaskLog implements ITaskLog {

    /**
     * Start time of the task (see {@link System#nanoTime()})
     */
    private final long start;

    /**
     * Is the task finished?
     */
    private boolean finished = false;

    /**
     * Constructor: init start time
     */
    public ConsoleTaskLog() {
        this.start = System.nanoTime();
    }

    /**
     * End the task: write a message into the console then the task spent time
     */
    @Override
    public void close() {
        log.info("Task ended in: " + NanoTimeHelpers.nanoToHuman(System.nanoTime() - this.start));
        this.finished = true;
    }

    /**
     * Write a message into the console
     *
     * @param message The message to write
     */
    @Override
    public void setMessage(final String message) {
        if (this.finished) {
            throw new IllegalStateException("Cannot set a message to a finished task.");
        }
        log.info("Start task: " + message);
    }

}
