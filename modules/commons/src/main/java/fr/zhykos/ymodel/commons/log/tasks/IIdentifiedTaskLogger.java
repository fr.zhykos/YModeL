/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.log.tasks;

/**
 * Identify a task logger
 */
public interface IIdentifiedTaskLogger {

    /**
     * Identify itself, a task, into a logger which automatically compute the task
     * time. Default call {@link #identifyTaskLogger(ITaskLog, int, int)} with 1 and
     * 1 for the two last parameters.
     *
     * @param taskLog Log used to identify the task
     */
    default void identifyTaskLogger(ITaskLog taskLog) {
        identifyTaskLogger(taskLog, 1, 1);
    }

    /**
     * Identify itself, a task, into a logger which automatically compute the task
     * time
     *
     * @param taskLog    Log used to identify the task
     * @param taskNumber Task number: may be used to identify the task with a
     *                   number, like "[number/total]"
     * @param tasksTotal Tasks totals: may be used to set the total of all tasks,
     *                   like "[number/total]"
     */
    default void identifyTaskLogger(ITaskLog taskLog, int taskNumber, int tasksTotal) {
        // Do nothing
    }

}
