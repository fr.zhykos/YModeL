/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.log.tasks;

import fr.zhykos.ymodel.commons.log.tasks.impl.ConsoleTasksLogger;
import fr.zhykos.ymodel.commons.log.tasks.impl.NoTasksLogger;

/**
 * Builder for a Tasks Logger
 */
public final class TasksLoggerBuilder {

    /**
     * Internal type to chose the logger
     */
    private enum ELoggerType {
        /** No logger */
        NULL,
        /** Console logger */
        CONSOLE
    }

    /**
     * Selected logger type
     */
    private final ELoggerType loggerType;

    private TasksLoggerBuilder(final ELoggerType type) {
        this.loggerType = type;
    }

    /**
     * Activate console logging
     *
     * @return The builder
     */
    public static TasksLoggerBuilder console() {
        return new TasksLoggerBuilder(ELoggerType.CONSOLE);
    }

    /**
     * Deactivate logging
     *
     * @return The builder
     */
    public static TasksLoggerBuilder disable() {
        return new TasksLoggerBuilder(ELoggerType.NULL);
    }

    /**
     * Build the logger
     *
     * @return The new logger
     */
    public ITasksLogger build() {
        return switch (this.loggerType) {
            case CONSOLE -> new ConsoleTasksLogger();
            default -> new NoTasksLogger();
        };
    }

}
