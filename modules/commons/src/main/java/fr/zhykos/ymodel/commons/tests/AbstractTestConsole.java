/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.tests;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

/**
 * Util class to extend if you want to assert console from {@link System}
 */
public abstract class AbstractTestConsole {

    /**
     * Default {@link System#out}
     */
    private PrintStream systemOut;

    /**
     * Default {@link System#err}
     */
    private PrintStream systemErr;

    /**
     * Replace the default {@link System#out} with this stream
     */
    private ByteArrayOutputStream sysOutStream;

    /**
     * Replace the default {@link System#err} with this stream
     */
    private ByteArrayOutputStream sysErrStream;

    /**
     * Replace the {@link System} out and err streams with internal one we can
     * eventually export
     */
    public void replaceStreams() {
        this.systemOut = System.out; // NOSONAR used for junits
        this.systemErr = System.err; // NOSONAR used for junits

        this.sysOutStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(this.sysOutStream, true, StandardCharsets.UTF_8));

        this.sysErrStream = new ByteArrayOutputStream();
        System.setErr(new PrintStream(this.sysErrStream, true, StandardCharsets.UTF_8));
    }

    /**
     * Reset {@link System} out and err streams
     */
    public void resetStreams() {
        System.setOut(this.systemOut);
        System.setErr(this.systemErr);
    }

    /**
     * @return The content of the replaced {@link System#out}
     */
    public String getSystemOut() {
        return this.sysOutStream.toString(StandardCharsets.UTF_8).trim();
    }

    /**
     * @return The content of the replaced {@link System#err}
     */
    public String getSystemError() {
        return this.sysErrStream.toString(StandardCharsets.UTF_8).trim();
    }

}
