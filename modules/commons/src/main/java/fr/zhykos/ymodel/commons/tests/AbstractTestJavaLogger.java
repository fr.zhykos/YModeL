/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/**
 * Util class to extend if you want to assert Java logger
 * {@link java.util.logging.Logger}
 */
public abstract class AbstractTestJavaLogger {

    /**
     * Default handlers from the logger specified within the constructor
     */
    private final Handler[] handlers;

    /**
     * The handler to use to assert logs (replaces default handlers)
     */
    private final TestHandler testHandler;

    /**
     * The logger to assert
     */
    private final Logger logger;

    /**
     * Constructor
     *
     * @param loggerName Logger name to assert
     */
    protected AbstractTestJavaLogger(final String loggerName) {
        this.logger = Logger.getLogger(loggerName);
        this.handlers = logger.getHandlers();
        this.testHandler = new TestHandler();
    }

    /**
     * Replace the logger handlers with the internal {@link TestHandler}
     */
    public void replaceHandlers() {
        this.testHandler.logs.clear();
        Arrays.stream(this.handlers).forEach(logger::removeHandler);
        logger.addHandler(this.testHandler); // NOSONAR used for junits
    }

    /**
     * Reset handlers: replace the {@link TestHandler} with the default from the
     * logger
     */
    public void resetHandlers() {
        logger.removeHandler(this.testHandler);
        Arrays.stream(this.handlers).forEach(logger::addHandler);
    }

    /**
     * @return The saved logged messages from the {@link TestHandler}
     */
    public List<String> getLogMessages() {
        return Collections.unmodifiableList(this.testHandler.logs);
    }

    /**
     * Specific log handler to save logged messages
     */
    private static class TestHandler extends StreamHandler {
        /**
         * Logged messages
         */
        private final List<String> logs = new ArrayList<>();

        @Override
        public synchronized void publish(final LogRecord logRecord) {
            final String msg = logRecord.getMessage();
            this.logs.add(msg);
            flush();
        }
    }

}
