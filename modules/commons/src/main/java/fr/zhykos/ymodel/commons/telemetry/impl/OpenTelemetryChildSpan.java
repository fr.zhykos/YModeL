/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.telemetry.impl;

import fr.zhykos.ymodel.commons.telemetry.IChildSpan;
import io.opentelemetry.api.trace.Span;

/**
 * OpenTelemetry Span
 */
public final class OpenTelemetryChildSpan implements IChildSpan {

    /**
     * Wrapped span
     */
    private final Span otlpSpan;

    /**
     * Is the span closed?
     */
    private boolean closed = false;

    /**
     * Constructor
     *
     * @param span The OpenTelemetry span used in this class
     */
    public OpenTelemetryChildSpan(final Span span) {
        this.otlpSpan = span;
    }

    @Override
    public void close() {
        this.otlpSpan.end();
        this.closed = true;
    }

    @Override
    public void setAttribute(final String key, final String value) {
        if (this.closed) {
            throw new IllegalStateException("You cannot set an attribute to a closed span.");
        }
        this.otlpSpan.setAttribute(key, value);
    }

}
