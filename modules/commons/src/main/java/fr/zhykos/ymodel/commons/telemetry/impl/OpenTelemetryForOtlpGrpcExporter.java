/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.commons.telemetry.impl;

import fr.zhykos.ymodel.commons.telemetry.IChildSpan;
import fr.zhykos.ymodel.commons.telemetry.ITelemetry;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.StatusCode;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.api.trace.propagation.W3CTraceContextPropagator;
import io.opentelemetry.context.propagation.ContextPropagators;
import io.opentelemetry.exporter.otlp.metrics.OtlpGrpcMetricExporter;
import io.opentelemetry.exporter.otlp.trace.OtlpGrpcSpanExporter;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.metrics.SdkMeterProvider;
import io.opentelemetry.sdk.metrics.export.PeriodicMetricReader;
import io.opentelemetry.sdk.resources.Resource;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.trace.export.BatchSpanProcessor;
import io.opentelemetry.semconv.resource.attributes.ResourceAttributes;

/**
 * Telemetry implementation for OpenTelemetry
 */
public final class OpenTelemetryForOtlpGrpcExporter implements ITelemetry {

    /**
     * OpenTelemetry tracer
     */
    private final Tracer tracer;

    /**
     * Constructor
     *
     * @param otlpEndpoint     OpenTelemetry agent URL
     * @param serviceName      Service name (used to identify the tracer service, to
     *                         regroup all traces into the same service)
     * @param globalTracerName Global tracer name (the one created globally, for
     *                         instance in your API, which creates the main span)
     */
    public OpenTelemetryForOtlpGrpcExporter(final String otlpEndpoint, final String serviceName,
            final String globalTracerName) {
        final io.opentelemetry.api.OpenTelemetry openTelemetry = init(otlpEndpoint, serviceName);
        this.tracer = openTelemetry.getTracer(globalTracerName);
    }

    @Override
    public IChildSpan createChildSpan(final String name) {
        final Span span = tracer.spanBuilder(name).startSpan();
        return new OpenTelemetryChildSpan(span);
    }

    @Override
    public void recordException(final Exception exception) {
        Span.current().recordException(exception);
        Span.current().setStatus(StatusCode.ERROR);
    }

    private static io.opentelemetry.api.OpenTelemetry init(final String otlpEndpoint, final String serviceName) {
        final SdkTracerProvider sdkTracerProvider = SdkTracerProvider
            .builder()
            .setResource(Resource.create(Attributes.of(ResourceAttributes.SERVICE_NAME, serviceName)))
            .addSpanProcessor(BatchSpanProcessor
                .builder(OtlpGrpcSpanExporter.builder().setEndpoint(otlpEndpoint).build())
                .build())
            .build();

        final SdkMeterProvider sdkMeterProvider = SdkMeterProvider
            .builder()
            .registerMetricReader(PeriodicMetricReader
                .builder(OtlpGrpcMetricExporter.builder().setEndpoint(otlpEndpoint).build())
                .build())
            .build();

        return OpenTelemetrySdk
            .builder()
            .setTracerProvider(sdkTracerProvider)
            .setMeterProvider(sdkMeterProvider)
            .setPropagators(ContextPropagators.create(W3CTraceContextPropagator.getInstance()))
            .build();
    }

}
