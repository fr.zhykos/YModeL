/*
 * Copyright 2022 Thomas "Zhykos" Cicognani.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.zhykos.ymodel.presentation.cli;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.concurrent.Callable;

import fr.zhykos.ymodel.commons.annotations.JacocoExclusionEvenItSNotGenerated;
import fr.zhykos.ymodel.commons.log.tasks.ITasksLogger;
import fr.zhykos.ymodel.commons.log.tasks.TasksLoggerBuilder;
import fr.zhykos.ymodel.commons.telemetry.impl.NoTelemetry;
import fr.zhykos.ymodel.core.features.GenerateZipFromYamlDefinitionService;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * Service to generate a metamodel: uses Command Line Interface
 */
@Command(name = "YMF", mixinStandardHelpOptions = true, version = "YModeL 1.0", description = "Generate metamodel classes from a YML declaration file.")
public final class YMFCLI implements Callable<Integer> {

    /**
     * The YML metamodel description file
     */
    @Option(names = { "-y", "-i", "--yml",
            "--yaml" }, description = "The YML metamodel description file.", required = true)
    private File ymlFile;

    /**
     * Target zip file
     */
    @Option(names = { "-z", "--zip" }, description = "Target zip file", required = true)
    private File zipFile;

    /**
     * Target language in which metamodel classes will be generated
     */
    @Option(names = { "-l",
            "--language" }, description = "Target language in which metamodel classes will be generated", required = true)
    private String targetLanguage;

    /**
     * Main: application entry point.
     * Exits {@link CommandLine.ExitCode#OK} if execution succeeded
     *
     * @param args Application arguments
     */
    @JacocoExclusionEvenItSNotGenerated
    public static void main(final String[] args) {
        System.exit(mainWithoutExit(args));
    }

    /**
     * Main: application entry point.
     *
     * @param args Application arguments
     * @return Exit code: {@link CommandLine.ExitCode#OK} if execution succeeded
     */
    public static int mainWithoutExit(final String[] args) {
        return new CommandLine(new YMFCLI()).execute(args);
    }

    @Override
    public Integer call() throws Exception {
        try (FileOutputStream stream = new FileOutputStream(this.zipFile)) {
            final String ymlContent = Files.readString(this.ymlFile.toPath());
            final ITasksLogger tasksLogger = TasksLoggerBuilder.console().build();
            new GenerateZipFromYamlDefinitionService(new NoTelemetry(), tasksLogger)
                .generate(ymlContent, this.targetLanguage, stream);
            return CommandLine.ExitCode.OK;
        }
    }

}
