- ne plus sortir de zip
- supprimer createGenerator de GenerateZipFromYamlDefinitionService. Mettre la méthode dans YMFFeature et passer la String target au lieu du générateur service

- yml en yaml
- Renommer en YMF
- api de validation
- renommer MetamodelController>>generateMetamodel en generateZipFromYamlDefinitionFile

- typescript "à la emf"

- site : $i18n.getString( "site-renderer", $locale, "template.lastpublished" ): $dateValue| $i18n.getString( "site-renderer", $locale, "template.version" ): 0.0.1-SNAPSHOT dans le header
- revoir toutes les erreurs spotbugs, notamment les EI_EXPOSE_REP pour essayer de bien tout corriger et non plus exclure les erreurs

- input : graphml, emf
- fichier à la genmodel (dossiers d'export, langage target...)
- cible de génération : BO, DTO, Entities + mappers

- maven plugin pour générer le metamodel
- npm pour générer le metamodel

- équivalent codeql pour la CI
- checkstyle ou PMD ne voient pas les final manquants au niveau des paramètres de méthode
- essayer de mettre un cache pour sonar quand on le lance sur gitlab ci (pour réduire le temps de traitement grandement pris par owasp)
- graal vm https://www.graalvm.org/22.1/reference-manual/native-image/#install-native-image

- vérifier versions dépendance (de temps en temps)